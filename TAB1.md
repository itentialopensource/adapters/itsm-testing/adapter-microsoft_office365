# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the MicrosoftOffice365 System. The API that was used to build the adapter for MicrosoftOffice365 is usually available in the report directory of this adapter. The adapter utilizes the MicrosoftOffice365 API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Microsoft Office 365 adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Microsoft Office 365. 
With this adapter you have the ability to perform operations with Microsoft Office 365 on items such as:

- Groups
- Users

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
