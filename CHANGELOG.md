
## 0.3.4 [10-15-2024]

* Changes made at 2024.10.14_20:32PM

See merge request itentialopensource/adapters/adapter-microsoft_office365!14

---

## 0.3.3 [09-01-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-microsoft_office365!12

---

## 0.3.2 [08-14-2024]

* Changes made at 2024.08.14_18:45PM

See merge request itentialopensource/adapters/adapter-microsoft_office365!11

---

## 0.3.1 [08-07-2024]

* Changes made at 2024.08.06_19:58PM

See merge request itentialopensource/adapters/adapter-microsoft_office365!10

---

## 0.3.0 [07-05-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/itsm-testing/adapter-microsoft_office365!9

---

## 0.2.7 [03-27-2024]

* Changes made at 2024.03.27_14:04PM

See merge request itentialopensource/adapters/itsm-testing/adapter-microsoft_office365!8

---

## 0.2.6 [03-21-2024]

* Changes made at 2024.03.21_14:40PM

See merge request itentialopensource/adapters/itsm-testing/adapter-microsoft_office365!7

---

## 0.2.5 [03-13-2024]

* Changes made at 2024.03.13_15:05PM

See merge request itentialopensource/adapters/itsm-testing/adapter-microsoft_office365!6

---

## 0.2.4 [03-11-2024]

* Changes made at 2024.03.11_14:46PM

See merge request itentialopensource/adapters/itsm-testing/adapter-microsoft_office365!5

---

## 0.2.3 [02-28-2024]

* Changes made at 2024.02.28_11:11AM

See merge request itentialopensource/adapters/itsm-testing/adapter-microsoft_office365!4

---

## 0.2.2 [12-25-2023]

* update axios and metadata

See merge request itentialopensource/adapters/itsm-testing/adapter-microsoft_office365!3

---

## 0.2.1 [12-14-2023]

* Remediation Merge Request

See merge request itentialopensource/adapters/itsm-testing/adapter-microsoft_office365!2

---

## 0.2.0 [11-11-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/itsm-testing/adapter-microsoft_office365!1

---

## 0.1.1 [08-04-2021]

* Bug fixes and performance improvements

See commit 61048b8

---
