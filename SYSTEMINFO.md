# Microsoft Office 365

Vendor: Microsoft
Homepage: https://www.microsoft.com/en-us/

Product: Microsoft Office 365
Product Page: https://www.microsoft.com/en-us/microsoft-365

## Introduction
We classify Microsoft Office 365 into the ITSM or Service Management domain as Microsoft Office 365 provides the ability to support and enhance various aspects of IT service delivery within an organization.


## Why Integrate
The Microsoft Office 365 adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Microsoft Office 365. 
With this adapter you have the ability to perform operations with Microsoft Office 365 on items such as:

- Groups
- Users

## Additional Product Documentation
The [API documents for Microsoft Office 365](https://learn.microsoft.com/en-us/previous-versions/office/office-365-api/)
